// Our Services tabs
const serviceTabList = document.querySelectorAll(".service-tab-title");
const serviceContent = document.querySelectorAll(".tab-content");

function changeServiceSectionTab () {
    let tabName;

    serviceTabList.forEach(tab => {
        tab.classList.remove("service-tab-active");
    });

    this.classList.add("service-tab-active");

    tabName = this.getAttribute("data-tab-name");

    changeServiceSectionContent(tabName);

}

function changeServiceSectionContent(tabName) {

    serviceContent.forEach(content => {

        if (content.classList.contains(tabName)) {
            content.id = "content-active";

        } else {
            content.id = "";

        }
    });
}

serviceTabList.forEach(tab => {
    tab.addEventListener('click', changeServiceSectionTab);
});




//     Our Amazing Work - filter
const ourAmazingWorkSectionContent = {
    10: `<li class="amazing-work-filter-content wordpress">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/wordpress-1.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        wordpress
                                    </div>
                                </div>
                            </li>`,
    11: `<li class="amazing-work-filter-content wordpress">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/wordpress-2.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        wordpress
                                    </div>
                                </div>
                            </li>`,
    12: `<li class="amazing-work-filter-content wordpress">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/wordpress-3.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        wordpress
                                    </div>
                                </div>
                            </li>`,
    5: `<li class="amazing-work-filter-content web-design">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/web-design-1.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        web design
                                    </div>
                                </div>
                            </li>`,
    6: `<li class="amazing-work-filter-content web-design">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/web-design-2.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        web design
                                    </div>
                                </div>
                            </li>`,
    7: `<li class="amazing-work-filter-content web-design">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/web-design-3.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        web design
                                    </div>
                                </div>
                            </li>`,
    1: `<li class="amazing-work-filter-content graphic-design">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/graphic-design-1.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        graphic design
                                    </div>
                                </div>
                            </li>`,
    2: `<li class="amazing-work-filter-content graphic-design">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/graphic-design-2.jpg" alt="img">
                                </div>
                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        graphic design
                                    </div>
                                </div>
                            </li>`,
    3: `<li class="amazing-work-filter-content graphic-design">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/graphic-design-3.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        graphic design
                                    </div>
                                </div>
                            </li>`,
    4: `<li class="amazing-work-filter-content graphic-design">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/graphic-design-4.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        graphic design
                                    </div>
                                </div>
                            </li>`,
    8: `<li class="amazing-work-filter-content landing-pages">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/landing-pages-1.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        landing
                                    </div>
                                </div>
                            </li>`,
    9: `<li class="amazing-work-filter-content landing-pages">
                                <div  class="">
                                    <img class="amazing-work-content-img graphic-design" src="./img/landing-pages-2.jpg" alt="img">
                                </div>

                                <div class="amazing-work-content-details">
                                    <div class="amazing-work-content-links">
                                        <a class="first-link" href="#"><span></span></a>
                                        <a class="second-link" href="#"></a>
                                    </div>
                                    <div class="amazing-work-content-title">
                                        creative design
                                    </div>
                                    <div class="category-name">
                                        landing
                                    </div>
                                </div>
                            </li>`,
}
const ourAmazingWorkSectionTabList = document.querySelectorAll(".amazing-work-filter-tab-list li");
const amazingWorkSectionContentList = document.querySelector(".amazing-work-filter-content-list");


function changeAmazingWorkSectionTab () {
    let amazingWorkCategory;

    ourAmazingWorkSectionTabList.forEach(tab => {
        tab.classList.remove("our-amazing-work__active-filter-tab");
    });

    this.classList.add("our-amazing-work__active-filter-tab");
    amazingWorkCategory = this.getAttribute("data-filter-category");

    changeAmazingWorkSectionContent(amazingWorkCategory);

}

function changeAmazingWorkSectionContent(category = "all") {

    for (let content of amazingWorkSectionContentList.children) {

        if (category === "all") {
            content.classList.add("amazing-work-active-content");
        } else
        if (content.classList.contains(category)) {

            content.classList.add("amazing-work-active-content");
        } else {
            content.classList.remove("amazing-work-active-content");

        }
    }
}

ourAmazingWorkSectionTabList.forEach(tab => {
    tab.addEventListener('click', changeAmazingWorkSectionTab);
});


const amazingWorkSectionLoadMoreBtn = document.querySelector('.button-amazing-work');
const amazingWorkLoadMoreBtnAnimation = document.querySelector('.button-amazing-work-load-animation');


// Our Amazing Work - Load More Button
amazingWorkSectionLoadMoreBtn.addEventListener('click', () => {
    amazingWorkLoadMoreBtnAnimation.style.visibility = "visible";
    amazingWorkSectionLoadMoreBtn.style.color = "transparent";
    amazingWorkSectionLoadMoreBtn.classList.add("pseudo-hide");


    setTimeout(() => {
        amazingWorkSectionLoadMoreBtn.style.display = "none";
        amazingWorkLoadMoreBtnAnimation.style.display = "none";

        let category = "all";
        ourAmazingWorkSectionTabList.forEach(tab => {

            if (tab.classList.contains('our-amazing-work__active-filter-tab')) {
                category = tab.getAttribute("data-filter-category");

            }

        })

        for (let key in ourAmazingWorkSectionContent) {
            amazingWorkSectionContentList.innerHTML += ourAmazingWorkSectionContent[key];
        }

        for (const li of amazingWorkSectionContentList.children) {
            if (li.classList.contains(category)) {
                li.classList.add("amazing-work-active-content");

            } else
                if (category === "all") {
                    li.classList.add("amazing-work-active-content");
                }
        }
    }, 1500);
});

////////// What People Say About theHam Section
// people list - info for slider
const peopleList = {
    0: {
        idName: 'philip-martin',
        name: 'Philip Martin',
        quote: 'Lorem ipsum dolor sit amet, consectetur adipisicingLorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, vitae! elit. Inventore, vitae!',
        position: 'UX Designer',
        photo: './img/philip-martin.jpg'
    },
    1: {
        idName: 'john-deer',
        name: 'John Deer',
        quote: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, vitae!',
        position: 'Designer',
        photo: './img/john-deer.jpeg'
    },
    2: {
        idName: 'bob-dobran',
        name: 'Bob Dobran',
        quote: 'Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur adipisicing elit. Inventore, vitae!',
        position: 'UX Designer',
        photo: './img/bob-dobran.jpg'
    },
    3: {
        idName: 'martin-philip',
        name: 'Martin Philip',
        quote: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam atque corporis doloremque exercitationem expedita hic id itaque, magnam maxime, modi necessitatibus nesciunt nobis perferendis quaerat quibusdam quo, repudiandae similique ut.\n',
        position: 'Designer',
        photo: './img/martin-philip.jpg'
    }
}
const peopleImgListSlider = document.querySelectorAll('.what-people-say-section__people-img div');
const leftSliderBtn = document.querySelector('.what-people-say__left-slider-btn');
const rightSliderBtn = document.querySelector('.what-people-say__right-slider-btn');
const whatPeopleSaySection = document.querySelectorAll('.what-people-say-section');


function getActiveStatus() {
    let position = 0;
    peopleImgListSlider.forEach((value, key, parent) => {
        if (value.classList.contains('slider-active-photo')) {
            position = key;
        }
    });
    return position;
}

function initQuote(idName) {
    const authorQuote = document.querySelector('.what-people-say__author-quote');
    const authorName = document.querySelector('.what-people-say__author-name');
    const authorPosition = document.querySelector('.what-people-say__author-position');
    const authorPhoto = document.querySelector('.what-people-say__author-photo img');

    for (let peopleKey in peopleList) {
        if (idName === peopleList[peopleKey].idName) {
            authorQuote.innerHTML = peopleList[peopleKey].quote;
            authorName.innerHTML = peopleList[peopleKey].name;
            authorPosition.innerHTML = peopleList[peopleKey].position;
            authorPhoto.src = peopleList[peopleKey].photo;

        }

    }

}

function stepLeft() {
    let position = getActiveStatus();
    if (position === 0) {
        peopleImgListSlider[position].classList.remove('slider-active-photo');
        peopleImgListSlider[peopleImgListSlider.length - 1].classList.add("slider-active-photo");
        initQuote(peopleImgListSlider[peopleImgListSlider.length - 1].id);
    } else {
        peopleImgListSlider[position].classList.remove('slider-active-photo');
        peopleImgListSlider[position - 1].classList.add("slider-active-photo");
        initQuote(peopleImgListSlider[position - 1].id);
    }

}

function stepRight() {
    let position = getActiveStatus();
    if (position === peopleImgListSlider.length - 1) {
        peopleImgListSlider[position].classList.remove('slider-active-photo');
        peopleImgListSlider[0].classList.add("slider-active-photo");
        initQuote(peopleImgListSlider[0].id);
    } else {
        peopleImgListSlider[position].classList.remove('slider-active-photo');
        peopleImgListSlider[position + 1].classList.add("slider-active-photo");
        initQuote(peopleImgListSlider[position + 1].id);
    }

}

function moveLeft(event) {
    if (event.code === "ArrowLeft" || event.type === 'click') {
        stepLeft();
    }
    // if (event.type === 'click') {
    //     stepLeft()
    // }
}

function moveRight(event) {
    if (event.code === "ArrowRight" || event.type === 'click') {
        stepRight();

    }
    // if (event.type === 'click') {
    //     stepRight();
    // }
}


leftSliderBtn.addEventListener("click", moveLeft);
leftSliderBtn.addEventListener("mousedown", () => {
    leftSliderBtn.style.background = "#18CFAB";
    document.addEventListener("mouseup", () => {
        leftSliderBtn.style.background = "transparent";
    });
});


rightSliderBtn.addEventListener("click", moveRight);
rightSliderBtn.addEventListener("mousedown", () => {
    rightSliderBtn.style.background = "#18CFAB";
    document.addEventListener("mouseup", () => {
        rightSliderBtn.style.background = "transparent";
    });
});


document.addEventListener("keydown", (event) => {
    if (event.code === "ArrowLeft") {
        leftSliderBtn.style.background = "#18CFAB";
        document.addEventListener("keyup", () => {
            leftSliderBtn.style.background = "transparent";
        });
    } else if (event.code === "ArrowRight") {
        rightSliderBtn.style.background = "#18CFAB";
        document.addEventListener("keyup", () => {
            rightSliderBtn.style.background = "transparent";
        });
    }
});


function mouseActivePeopleImg () {
    peopleImgListSlider.forEach(peopleImg => {
        peopleImg.classList.remove("slider-active-photo");
    });
    this.classList.add("slider-active-photo");
    initQuote(this.id);
}

peopleImgListSlider.forEach(peopleImg => {
    peopleImg.addEventListener('click', mouseActivePeopleImg);
});
const whatPeopleSaySectionObserver = new IntersectionObserver((entries, observer) => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            document.addEventListener('keyup', moveLeft);
            document.addEventListener('keyup', moveRight);
        } else {
            document.removeEventListener('keyup', moveLeft);
            document.removeEventListener('keyup', moveRight);
        }
    })
    }, {})

whatPeopleSaySection.forEach(value => {
    whatPeopleSaySectionObserver.observe(value);
});